from django.shortcuts import render
from django.http import HttpResponse
from django.conf import Settings
from django.conf.urls.static import static
from django.contrib.auth.models import User
from first_app.models import AccessRecord, Webpage, Topic, Contacts, Userprofileinfo
from . import forms
#from first_app.forms import FormsName,ContactFrom
# Create your views here.


def index(request):
    # return HttpResponse("<em>Hello World</em>")
    #template_var = {'body_text':"hello world from index fn templates first_app/index.html"}
    #fetch_data = AccessRecord.objects.order_by('date')
    fetch_data = AccessRecord.objects.all
    #fetch_data = Webpage.objects.all
    template_var = {'body_text':fetch_data}
    return render(request,'first_app/index.html',context=template_var)


def help(request):
    # return HttpResponse("<em>Hello World</em>")
    template_var = {'body_text':"help page"}
    return render(request,'first_app/help.html',context=template_var)

def customform(request):
    form = forms.FormsName

    if request.method == 'POST':
        form = forms.FormsName(request.POST)
        print("VALIDATION SUCCESS")

        if form.is_valid():
            print("VALIDATION SUCCESS 2")
            print(form.cleaned_data['name'])


    return render(request,'first_app/form.html',{'form':form})



def conform(request):
    form = forms.ContactForm()

    if request.method == 'POST':
        form = forms.ContactForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return conform(request)


    return render(request,'first_app/contact.html',{'form':form})


def registration(request):
    registered = False

    if request.method == 'POST':
        user_form = forms.UserForm(request.POST)
        profile_form = forms.UserprofileinfoForm(request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()

            profile = profile_form.save(commit=False)
            profile.user = user
            #if pro_pic in request.FILES:
            #    profile.pro_pic = request.FILES['pro_pic']
            profile.save()
            registered = True
        else:
            print(user_form.errors,profile_form.errors)
    else:
        user_form = forms.UserForm()
        profile_form = forms.UserprofileinfoForm()

    return render(request,'first_app/registration.html',{'user_form':user_form,'profile_form':profile_form,'registered':registered})
