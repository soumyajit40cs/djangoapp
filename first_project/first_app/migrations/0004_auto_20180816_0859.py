# Generated by Django 2.1 on 2018-08-16 08:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('first_app', '0003_userprofileinfo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofileinfo',
            name='pro_pic',
            field=models.ImageField(blank=True, upload_to='profile_pic/'),
        ),
    ]
